﻿namespace Android_Test_App_2
{
    class Manufacture
    {
        private int korg;
        private string norg;

        public Manufacture(int korg, string norg)
        {
            this.korg = korg;
            this.norg = norg;
        }

        public int Korg { get => korg; set => korg = value; }
        public string Norg { get => norg; set => norg = value; }

        public override string ToString()
        {
            return norg;
        }
    }
}