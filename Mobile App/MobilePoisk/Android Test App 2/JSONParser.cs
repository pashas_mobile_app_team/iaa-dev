﻿using System;
using Org.Json;
using System.IO;
using System.Net;
using Android.Net;
using Android.Content;
using Android.App;
using Android.Widget;

namespace Android_Test_App_2
{
    public class JSONParser
    {
        static JSONObject jObj = null;
        static String json = "";
        public JSONParser() { }

        public JSONObject makeHttpRequest(String url)
        {
            if (checkNetworkStatus())
            {
                HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(url);
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                using (StreamReader stream = new StreamReader(
                     resp.GetResponseStream()))
                {
                    json = stream.ReadToEnd();
                }
                jObj = new JSONObject(json);
                return jObj;
            }
            else
            {
                return null;
            }
        }

        public void makeHttpRequestWithoutResponse(String url)
        {
            if (checkNetworkStatus())
            {
                HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(url);
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
                using (StreamReader stream = new StreamReader(
                     resp.GetResponseStream()))
                {
                    json = stream.ReadToEnd();
                }
            }
        }

        private bool checkNetworkStatus()
        {
            var connectivityManager = (ConnectivityManager)(Application.Context.GetSystemService(Context.ConnectivityService));
            NetworkInfo networkInfo = connectivityManager.ActiveNetworkInfo;
            return networkInfo != null && networkInfo.IsConnected;
        }
    }
}