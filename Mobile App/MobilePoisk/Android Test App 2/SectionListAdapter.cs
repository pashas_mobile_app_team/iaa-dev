﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Views;
using Android.Widget;

namespace Android_Test_App_2
{
    class SectionAdapter : ProductsAdapter
    {
        public static Dictionary<string, List<Product>> _sections;

        public SectionAdapter(IList<Product> products) : base(products)
        {
            _sections = new Dictionary<string, List<Product>>();
            BuildSections();
        }

        void BuildSections()
        {
            _sections.Clear();
            foreach (var s in _product.GroupBy(item => item.Area).OrderBy(gr => gr.Key))
            {
                _sections.Add(s.Key, s.ToList());
            }
        }

        public override Product this[int position]
        {
            get
            {
                foreach (var section in _sections.Keys)
                {
                    var items = _sections[section];
                    int size = items.Count + 1;
                    if (position == 0) return null;
                    if (position < size) return items[position - 1];
                    position -= size;
                }
                return null;
            }
        }

        public override int Count
        {
            get
            {
                return base.Count + _sections.Count;

            }
        }

        public override int ViewTypeCount
        {
            get
            {
                return 2;
            }
        }

        public override int GetItemViewType(int position)
        {
            foreach (var section in _sections.Keys)
            {
                var items = _sections[section];
                int size = items.Count + 1;
                if (position == 0) return 0;
                if (position < size) return 1;
                position -= size;
            }
            throw new NullReferenceException();
        }

        public override void Remove(Product item)
        {
            base.Remove(item);
            BuildSections();
        }

        public override void Remove(IEnumerable<Product> items)
        {
            base.Remove(items);
            BuildSections();
        }

        public override Product GetRawItem(int position)
        {
            foreach (var section in _sections.Keys)
            {
                var items = _sections[section];
                int size = items.Count + 1;
                if (position == 0) return null;
                if (position < size) return items[position - 1];
                position -= size;
            }
            return null;
        }

        protected override void SelectView(int position, bool value)
        {
            var vt = GetItemViewType(position);
            if (vt == 0) return;
            base.SelectView(position, value);
        }

        string GetSection(int position)
        {
            foreach (var section in _sections.Keys)
            {
                var items = _sections[section];
                int size = items.Count + 1;
                if (position == 0 || (position < size)) return section;
                position -= size;
            }
            throw new NullReferenceException();
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var type = GetItemViewType(position);
            if (type == 0)
            {
                var view = convertView;
                if (view == null)
                {
                    view = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.header, parent, false);
                    var sectionViewHolder = new SectionViewHolder();
                    sectionViewHolder.Text = view.FindViewById<TextView>(Resource.Id.list_header_title);
                    view.Tag = sectionViewHolder;
                }
                var holder = (SectionViewHolder)view.Tag;
                holder.Text.Text = GetSection(position);
                return view;
            }
            else
            {
                var item = this[position];
                var view = convertView;
                if (view == null)
                {
                    view = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.productLayout, parent, false);
                    var viewHolder = new ViewHolder();
                    viewHolder.Text = view.FindViewById<TextView>(Resource.Id.row_custom_name);
                    view.Tag = viewHolder;
                }
                var holder = (ViewHolder)view.Tag;
                holder.Text.Text = item.Name;
                return view;
            }
        }

        class ViewHolder : Java.Lang.Object
        {
            public TextView Text { get; set; }
            public TextView Weight { get; set; }
            public ImageView Icon { get; set; }
        }

        class SectionViewHolder : Java.Lang.Object
        {
            public TextView Text { get; set; }
        }
    }
}