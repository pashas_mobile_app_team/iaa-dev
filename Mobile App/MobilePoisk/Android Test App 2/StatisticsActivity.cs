﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Widget;
using Org.Json;

namespace Android_Test_App_2
{
    [Activity(Label = "StatisticsActivity", ScreenOrientation = ScreenOrientation.Portrait)]
    public class StatisticsActivity : Activity
    {
        public ProgressDialog progress;
        private ListView view;
        private static String urlStatistic = "http://ips.mshp.gov.by/andr_mob/getStatistic.php";
        // JSON Node names
        private static String TAG_STATISTICS = "statistics";
        private static String TAG_NORG = "NORG";
        private static String TAG_DATE = "DATE";
        private static String TAG_COUNT = "COUNT";
        private static String TAG_PROVINCE = "PROVINCE";
        private JSONParser jParser = new JSONParser();
        JSONArray products = null;
        Dictionary<int, Statistic> parameters;

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.result);
            createProgressBar();
            await doInBackgroundAsync();
            view = FindViewById<ListView>(Resource.Id.results);
            view.ItemClick += View_ItemClick;
            view.Adapter = new StatisticsSectionAdapter(GetStatistics());
        }

        private void View_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            Statistic product = null;
            int temp = 1;
            Dictionary<string, List<Statistic>> pairs = StatisticsSectionAdapter._sections;
            foreach (KeyValuePair<string, List<Statistic>> keyValue in pairs)
            {
                if (product != null)
                {
                    break;
                }
                for (int i = 0; i < keyValue.Value.Count; i++)
                {
                    if (product != null)
                    {
                        break;
                    }
                    if (temp == e.Id)
                    {
                        product = keyValue.Value[i]; break;
                    }
                    temp++;
                }
                temp++;
            }
            if (product != null)
            {
                Intent intent = new Intent(this, typeof(AboutStatActivity));
                intent.PutExtra("product", (int)e.Id);
                StartActivity(intent);
            }
        }

        IList<Statistic> GetStatistics()
        {
            var list = new List<Statistic>();
            foreach (KeyValuePair<int, Statistic> keyValue in parameters)
            {
                list.Add(keyValue.Value);
            }
            return list;
        }

        private void createProgressBar()
        {
            progress = new ProgressDialog(this);
            progress.Indeterminate = true;
            progress.SetProgressStyle(ProgressDialogStyle.Spinner);
            progress.SetMessage("Пожалуйста, подождите. Идет загрузка...");
            progress.SetCancelable(false);
        }

        private async Task doInBackgroundAsync()
        {
            progress.Show();
            Task task = new Task(doInBackground);
            task.Start();
            await task;
            progress.Hide();
        }

        public void doInBackground()
        {
            parameters = new Dictionary<int, Statistic>();
            JSONObject json = jParser.makeHttpRequest(urlStatistic);
            if (json == null)
            {
                return;
            }
            try
            {
                products = json.GetJSONArray(TAG_STATISTICS);
                int ar = products.Length();
                for (int i = 0; i < products.Length(); i++)
                {
                    JSONObject c = products.GetJSONObject(i);
                    string norg = c.GetString(TAG_NORG);
                    String date = c.GetString(TAG_DATE);
                    int count = c.GetInt(TAG_COUNT);
                    int provinceID = c.GetInt(TAG_PROVINCE);
                    string province = "";
                    switch (provinceID)
                    {
                        case 1: province = "Брестская область"; break;
                        case 2: province = "Витебская область"; break;
                        case 3: province = "Гомельская область"; break;
                        case 4: province = "Гродненская область"; break;
                        case 5: province = "Минская область"; break;
                        case 6: province = "Могилевская область"; break;
                        case 7: province = "Город Минск"; break;
                    }
                    parameters.Add(i + 1, new Statistic(norg, date.Remove(10, 9), province, count));
                }
            }
            catch (JSONException e)
            {
                e.PrintStackTrace();
            }
        }
    }
}