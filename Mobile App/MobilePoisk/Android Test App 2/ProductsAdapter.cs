﻿using System.Collections.Generic;
using Android.Views;
using Android.Widget;

namespace Android_Test_App_2
{
    class ProductsAdapter : BaseAdapter<Product>
    {
        protected IList<Product> _product;
        
        public ProductsAdapter(IList<Product> products)
        {
            _product = products;
        }

        public override Product this[int position]
        {
            get
            {
                return _product[position];
            }
        }

        public override int Count
        {
            get
            {
                return _product.Count;
            }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.productLayout, parent, false);
            var name = view.FindViewById<TextView>(Resource.Id.row_custom_name);

            name.Text = _product[position].Name;
            return view;
        }

        public virtual void Remove(Product item)
        {
            _product.Remove(item);
        }

        public virtual void Remove(IEnumerable<Product> items)
        {
            foreach (var item in items)
            {
                _product.Remove(item);
            }
        }

        public virtual Product GetRawItem(int position)
        {
            return _product[position];
        }

        protected virtual void SelectView(int position, bool value) { }

        public virtual void RollBack() { }
    }
}