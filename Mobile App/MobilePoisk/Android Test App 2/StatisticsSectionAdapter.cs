﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Views;
using Android.Widget;

namespace Android_Test_App_2
{
    class StatisticsSectionAdapter : BaseAdapter<Statistic>
    {
        protected IList<Statistic> _product;
        public static Dictionary<string, List<Statistic>> _sections;

        public StatisticsSectionAdapter(IList<Statistic> products)
        {
            _product = products;
            _sections = new Dictionary<string, List<Statistic>>();
            BuildSections();
        }

        void BuildSections()
        {
            _sections.Clear();
            foreach (var s in _product.GroupBy(item => item.Area).OrderBy(gr => gr.Key))
            {
                _sections.Add(s.Key, s.ToList());
            }
        }

        public override Statistic this[int position]
        {
            get
            {
                foreach (var section in _sections.Keys)
                {
                    var items = _sections[section];
                    int size = items.Count + 1;
                    if (position == 0) return null;
                    if (position < size) return items[position - 1];
                    position -= size;
                }
                return null;
            }
        }

        public override int Count
        {
            get
            {
                return _product.Count + _sections.Count;
            }
        }

        public override int ViewTypeCount
        {
            get
            {
                return 2;
            }
        }

        public override int GetItemViewType(int position)
        {
            foreach (var section in _sections.Keys)
            {
                var items = _sections[section];
                int size = items.Count + 1;
                if (position == 0) return 0;
                if (position < size) return 1;
                position -= size;
            }
            throw new NullReferenceException();
        }

        public Statistic GetRawItem(int position)
        {
            foreach (var section in _sections.Keys)
            {
                var items = _sections[section];
                int size = items.Count + 1;
                if (position == 0) return null;
                if (position < size) return items[position - 1];
                position -= size;
            }
            return null;
        }

        protected void SelectView(int position, bool value)
        {
            var vt = GetItemViewType(position);
            if (vt == 0) return;
        }

        string GetSection(int position)
        {
            foreach (var section in _sections.Keys)
            {
                var items = _sections[section];
                int size = items.Count + 1;
                if (position == 0 || (position < size)) return section;
                position -= size;
            }
            throw new NullReferenceException();
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var type = GetItemViewType(position);
            if (type == 0)
            {
                var view = convertView;
                if (view == null)
                {
                    view = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.header, parent, false);
                    var sectionViewHolder = new SectionViewHolder();
                    sectionViewHolder.Text = view.FindViewById<TextView>(Resource.Id.list_header_title);
                    view.Tag = sectionViewHolder;
                }
                var holder = (SectionViewHolder)view.Tag;
                holder.Text.Text = GetSection(position);
                return view;
            }
            else
            {
                var item = this[position];
                var view = convertView;
                if (view == null)
                {
                    view = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.productLayout, parent, false);
                    var viewHolder = new ViewHolder();
                    viewHolder.Name = view.FindViewById<TextView>(Resource.Id.row_custom_name);
                    view.Tag = viewHolder;
                }
                var holder = (ViewHolder)view.Tag;
                holder.Name.Text = item.Name;
                return view;
            }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        class ViewHolder : Java.Lang.Object
        {
            public TextView Name { get; set; }
        }

        class SectionViewHolder : Java.Lang.Object
        {
            public TextView Text { get; set; }
        }
    }
}