﻿using System;
using System.Collections.Generic;
using Org.Json;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Android.Content.PM;
using System.Threading.Tasks;

namespace Android_Test_App_2
{
    [Activity(Label = "ResultActivity", ScreenOrientation = ScreenOrientation.Portrait)]
    public class ResultActivity : Activity
    {
        public ProgressDialog progress;
        private ListView view;
        private String urlGetGoodsViaAllfactory = "http://ips.mshp.gov.by/andr_mob/getGoodsViaAllFactory.php";
        private String urlGetGoodsViaFactory = "http://ips.mshp.gov.by/andr_mob/getGoodsViaFactory.php";
        private String urlGetGoodsViaProvince = "http://ips.mshp.gov.by/andr_mob/getGoodsViaProvince.php";
        private String urlCountViaProvince = "http://ips.mshp.gov.by/andr_mob/countViaProvince.php";
        private String urlCountViaAllFactory = "http://ips.mshp.gov.by/andr_mob/countViaAllFactory.php";
        private String urlCountViaFactory = "http://ips.mshp.gov.by/andr_mob/countViaFactory.php";
        private String urlSearchForWarehouses = "http://ips.mshp.gov.by/andr_mob/searchForWarehouses.php";
        public String query = "";
        public String searchFormat = "";
        public String credentials = "";
        private int countOfElements = 0;
        private int countL = 0;
        // JSON Node names
        private String TAG_PRODUCTS = "products";
        private String TAG_KOL = "KOL";
        private String TAG_NAME = "NAIM";
        private String TAG_CENA = "CENA";
        private String TAG_WAREHOUSE = "WAREHOUSE";
        private String TAG_PROVINCE = "PROVINCE";
        private String TAG_NORG = "NORG";
        private String TAG_TEL = "TEL";
        private String TAG_ADRESS = "ADRESS";
        private String TAG_MAILADR = "MAILADR";
        private String TAG_UNN = "UNN";
        private String TAG_RESPONS = "RESPONS";
        private String TAG_ADS = "ADS";
        private String TAG_NAMEW = "NAMEW";
        private String TAG_ADDRESSW = "ADDRESSW";
        private String TAG_CNT = "CNT";
        private JSONParser jParser = new JSONParser();
        JSONArray products = null;
        Dictionary<int, Product> parameters = new Dictionary<int, Product>();

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.result);
            try
            {
                query = Intent.GetStringExtra("query");
                searchFormat = Intent.GetStringExtra("searchFormat");
                credentials = Intent.GetStringExtra("credentials");
                view = FindViewById<ListView>(Resource.Id.results);
                createProgressBar();

                await doInBackgroundAsync();
                progress.Hide();
                view.Adapter = new SectionFactoryAdapter(GetProducts());

                getCount();
                view.ItemClick += View_ItemClick;
                view.Scroll += View_Scroll;
            }
            catch (Exception e)
            {
                Toast.MakeText(this, "ResultActivity", ToastLength.Long).Show();
            }
        }

        //[Android.Runtime.Register("onStart", "()V", "GetOnStartHandler")]
        //protected override async void OnStart()
        //{
        //    base.OnStart();
        //    try
        //    {
        //        await doInBackgroundAsync();
        //        view.Adapter = new SectionFactoryAdapter(GetProducts());
        //    }
        //    catch (Exception e)
        //    {

        //        Toast.MakeText(this, "При попытке получения товаров что-то пошло не так", ToastLength.Long).Show();
        //    }
        //}

        private void createProgressBar()
        {
            progress = new ProgressDialog(this);
            progress.Indeterminate = true;
            progress.SetProgressStyle(ProgressDialogStyle.Spinner);
            progress.SetMessage("Пожалуйста, подождите. Идет загрузка...");
            progress.SetCancelable(false);
        }

        private async Task doInBackgroundAsync()
        {
            progress.Show();
            Task task = new Task(doInBackground);
            task.Start();
            await task;
            //progress.Hide();
        }

        private void getCount()
        {
            JSONObject jobj = null;
            if (searchFormat == "obl")
            {
                switch (credentials)
                {
                    case "Брестская область": jobj = jParser.makeHttpRequest(urlCountViaProvince + "/?query=" + query + "&province=1").GetJSONArray(TAG_PRODUCTS).GetJSONObject(0); break;
                    case "Витебская область": jobj = jParser.makeHttpRequest(urlCountViaProvince + "/?query=" + query + "&province=2").GetJSONArray(TAG_PRODUCTS).GetJSONObject(0); break;
                    case "Гомельская область": jobj = jParser.makeHttpRequest(urlCountViaProvince + "/?query=" + query + "&province=3").GetJSONArray(TAG_PRODUCTS).GetJSONObject(0); break;
                    case "Гродненская область": jobj = jParser.makeHttpRequest(urlCountViaProvince + "/?query=" + query + "&province=4").GetJSONArray(TAG_PRODUCTS).GetJSONObject(0); break;
                    case "Минская область": jobj = jParser.makeHttpRequest(urlCountViaProvince + "/?query=" + query + "&province=5").GetJSONArray(TAG_PRODUCTS).GetJSONObject(0); break;
                    case "Могилевская область": jobj = jParser.makeHttpRequest(urlCountViaProvince + "/?query=" + query + "&province=6").GetJSONArray(TAG_PRODUCTS).GetJSONObject(0); break;
                    case "Город Минск": jobj = jParser.makeHttpRequest(urlCountViaProvince + "/?query=" + query + "&province=7").GetJSONArray(TAG_PRODUCTS).GetJSONObject(0); break;
                }
            }
            else
            {
                if (credentials == "Все производители")
                {
                    jobj = jParser.makeHttpRequest(urlCountViaAllFactory + "/?query=" + query).GetJSONArray(TAG_PRODUCTS).GetJSONObject(0);
                }
                else
                {
                    jobj = jParser.makeHttpRequest(urlCountViaFactory + "/?query=" + query + "&korg=" + searchFormat).GetJSONArray(TAG_PRODUCTS).GetJSONObject(0);
                }
            }
            if (jobj == null)
            {
                countL = 0;
                return;
            }
            countL = jobj.GetInt(TAG_CNT);
        }

        private async void View_Scroll(object sender, AbsListView.ScrollEventArgs e)
        {
            int visibleItemCount = e.VisibleItemCount;
            int totalItemCount = e.TotalItemCount;
            int firstVisibleItems = e.FirstVisibleItem;
            if (totalItemCount < countL)
            {
                if ((visibleItemCount + firstVisibleItems) >= totalItemCount)
                {
                    countOfElements = e.TotalItemCount;

                    await doInBackgroundAsync();
                    //doInBackground();
                    view.Adapter = new SectionFactoryAdapter(GetProducts());
                    view.SmoothScrollToPosition(view.LastVisiblePosition);
                    ListView l = (ListView)sender;
                    l.SetSelection(visibleItemCount + firstVisibleItems);
                }
            }
            progress.Hide();
        }

        private void View_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            Product product = null;
            int temp = 1;
            Dictionary<string, List<Product>> pairs;
            pairs = SectionFactoryAdapter._sections;
            foreach (KeyValuePair<string, List<Product>> keyValue in pairs)
            {
                if (product != null)
                {
                    break;
                }
                for (int i = 0; i < keyValue.Value.Count; i++)
                {
                    if (product != null)
                    {
                        break;
                    }
                    if (temp == e.Id)
                    {
                        product = keyValue.Value[i]; break;
                    }
                    temp++;
                }
                temp++;
            }
            if (product != null && !product.Name.Equals("Нет указанных товаров"))
            {
                Intent intent = new Intent(this, typeof(AboutProductActivity));
                intent.PutExtra("product", (int)e.Id);
                intent.PutExtra("credentials", credentials);
                StartActivity(intent);
            }
        }

        IList<Product> GetProducts()
        {
            var list = new List<Product>();
            foreach (KeyValuePair<int, Product> keyValue in parameters)
            {
                list.Add(keyValue.Value);
            }
            if (list.Count == 0)
            {
                list.Add(new Product("Нет указанных товаров", "Ошибка", "Ошибка", 0,"" , "", 0, "", "", "", "", "", 0));
            }
            return list;
        }

        public void doInBackground()
        {
            JSONObject json = null;
            if (searchFormat == "obl")
            {
                switch (credentials)
                {
                    case "Брестская область": json = jParser.makeHttpRequest(urlGetGoodsViaProvince + "/?query=" + query + "&province=1" + "&from=" + countOfElements); break;
                    case "Витебская область": json = jParser.makeHttpRequest(urlGetGoodsViaProvince + "/?query=" + query + "&province=2" + "&from=" + countOfElements); break;
                    case "Гомельская область": json = jParser.makeHttpRequest(urlGetGoodsViaProvince + "/?query=" + query + "&province=3" + "&from=" + countOfElements); break;
                    case "Гродненская область": json = jParser.makeHttpRequest(urlGetGoodsViaProvince + "/?query=" + query + "&province=4" + "&from=" + countOfElements); break;
                    case "Минская область": json = jParser.makeHttpRequest(urlGetGoodsViaProvince + "/?query=" + query + "&province=5" + "&from=" + countOfElements); break;
                    case "Могилевская область": json = jParser.makeHttpRequest(urlGetGoodsViaProvince + "/?query=" + query + "&province=6" + "&from=" + countOfElements); break;
                    case "Город Минск": json = jParser.makeHttpRequest(urlGetGoodsViaProvince + "/?query=" + query + "&province=7" + "&from=" + countOfElements); break;
                }
            }
            else
            {
                if (credentials == "Все производители")
                {
                    json = jParser.makeHttpRequest(urlGetGoodsViaAllfactory + "/?query=" + query);
                }
                else
                {
                    json = jParser.makeHttpRequest(urlGetGoodsViaFactory + "/?query=" + query + "&korg=" + searchFormat + "&from=" + countOfElements);
                }
            }

            if (json == null)
            {
                return;
            }
            try
            {
                products = json.GetJSONArray(TAG_PRODUCTS);
                for (int i = 0; i < products.Length(); i++)
                {
                    JSONObject c = products.GetJSONObject(i);

                    String name = c.GetString(TAG_NAME);
                    String cena = c.GetString(TAG_CENA);
                    int kol = c.GetInt(TAG_KOL);
                    String warehouseID = c.GetString(TAG_WAREHOUSE);
                    String norg = c.GetString(TAG_NORG);
                    String tel = c.GetString(TAG_TEL);
                    String adress = c.GetString(TAG_ADRESS);
                    String mailadr = c.GetString(TAG_MAILADR);
                    int unn = c.GetInt(TAG_UNN);
                    String respons = c.GetString(TAG_RESPONS);
                    String ads = c.GetString(TAG_ADS);
                    int provinceID = c.GetInt(TAG_PROVINCE);
                    String namew = "", addressw = "";
                    int ware = 0;
                    try
                    {
                        if (warehouseID != "null")
                        {
                            ware = int.Parse(warehouseID);
                            JSONObject json2 = jParser.makeHttpRequest(urlSearchForWarehouses + "/?query=" + warehouseID);
                            JSONArray array = json2.GetJSONArray("warehouses");

                            namew = array.GetJSONObject(0).GetString(TAG_NAMEW);
                            if (array.GetJSONObject(0).GetString(TAG_ADDRESSW) != "0")
                            {
                                addressw = array.GetJSONObject(0).GetString(TAG_ADDRESSW);
                            }
                            else
                            {
                                addressw = adress;
                            }
                            
                        }
                        else
                        {
                            addressw = adress;
                        }
                    }
                    catch (Exception)
                    {
                        addressw = adress;
                    }
                    string province = "";
                    switch (provinceID)
                    {
                        case 1: province = "Брестская область"; break;
                        case 2: province = "Витебская область"; break;
                        case 3: province = "Гомельская область"; break;
                        case 4: province = "Гродненская область"; break;
                        case 5: province = "Минская область"; break;
                        case 6: province = "Могилевская область"; break;
                        case 7: province = "Город Минск"; break;
                    }
                    parameters.Add(countOfElements + i + 1, new Product(name, norg, province, kol, cena, namew, unn, tel, addressw, mailadr, respons, ads, ware));
                }
            }
            catch (JSONException e)
            {
                e.PrintStackTrace();
            }
            catch (Exception e)
            {
                //Toast.MakeText(this, "doinbackgr", ToastLength.Long).Show();
            }
        }
    }
}