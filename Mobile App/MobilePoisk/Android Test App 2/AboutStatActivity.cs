﻿using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Widget;

namespace Android_Test_App_2
{
    [Activity(Label = "AboutStatActivity", ScreenOrientation = ScreenOrientation.Portrait)]
    public class AboutStatActivity : Activity
    {
        private ListView view;
        private Dictionary<string, List<Statistic>> pairs;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.aboutStat);
            view = FindViewById<ListView>(Resource.Id.aboutStat);
            int productID = Intent.GetIntExtra("product", 0);
            Statistic product = null;
            int temp = 1;
            pairs = StatisticsSectionAdapter._sections;
            foreach (KeyValuePair<string, List<Statistic>> keyValue in pairs)
            {
                if (product != null)
                {
                    break;
                }
                for (int i = 0; i < keyValue.Value.Count; i++)
                {
                    if (product != null)
                    {
                        break;
                    }
                    if (temp == productID)
                    {
                        product = keyValue.Value[i]; break;
                    }
                    temp++;
                }
                temp++;
            }
            List<string> list = new List<string>();
            list.AddRange(new string[]
            {
                "Производитель: " + product.Name,
                "Дата обновления: " + product.Date,
                "Позиции: " + product.Count,
            });
            var adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, list);
            view.Adapter = adapter;
        }
    }
}