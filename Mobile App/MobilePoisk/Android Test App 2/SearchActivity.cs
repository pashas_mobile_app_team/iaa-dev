﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using Org.Json;

namespace Android_Test_App_2
{
    [Activity(Label = "SearchActivity", ScreenOrientation = ScreenOrientation.Portrait)]
    public class SearchActivity : Activity
    {
        private JSONParser jParser = new JSONParser();
        private static String urlManufactures = "http://ips.mshp.gov.by/andr_mob/manufactures.php";
        private SearchView goodEditText;
        private Button searchButton;
        private Spinner spinner;
        private RadioButton obl;
        private RadioButton zavod;
        private static bool flag=false;
        IList<Manufacture> manufactures;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);
            searchButton = FindViewById<Button>(Resource.Id.buttonSearch);
            goodEditText = FindViewById<SearchView>(Resource.Id.good);
            obl = FindViewById<RadioButton>(Resource.Id.obl);
            zavod = FindViewById<RadioButton>(Resource.Id.zavod);
            searchButton.Visibility = ViewStates.Gone;
            searchButton.Click += Btn_Click;
            obl.Click += radio_ItemSelected;
            zavod.Click += radio_ItemSelected;
            spinner = FindViewById<Spinner>(Resource.Id.spinner);
            spinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(spinner_ItemSelected);
            var adapter = ArrayAdapter.CreateFromResource(
            this, Resource.Array.obls, Android.Resource.Layout.SimpleSpinnerItem);
            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinner.Adapter = adapter;
            flag = false;
        }

        protected override void OnResume()
        {
            base.OnResume();
            flag = false;
        }

        protected override void OnRestart()
        {
            base.OnRestart();
            flag = false;
        }

        private void radio_ItemSelected(object sender, EventArgs args)
        {
            RadioButton radio = (RadioButton)sender;
            if (radio.Text == "Выбор области")
            {
                var adapter = ArrayAdapter.CreateFromResource(
                    this, Resource.Array.obls, Android.Resource.Layout.SimpleSpinnerItem);

                adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
                spinner.Adapter = adapter;
            }
            else
            {
                getManufactures();
                var adapter = new ArrayAdapter<Manufacture>(this, Android.Resource.Layout.SimpleSpinnerItem, manufactures);
                spinner.Adapter = adapter;
            }
        }

        private void getManufactures()
        {
            manufactures = new List<Manufacture>();
            JSONObject json = jParser.makeHttpRequest(urlManufactures);
            if (json == null)
            {
                return;
            }
            try
            {
                JSONArray manuf = json.GetJSONArray("manufactures");
                manufactures.Add(new Manufacture(-3, "Все производители"));
                for (int i = 0; i < manuf.Length(); i++)
                {
                    JSONObject c = manuf.GetJSONObject(i);
                    String name = c.GetString("NORG");
                    int id = c.GetInt("KORG");
                    manufactures.Add(new Manufacture(id, name));
                }
            }
            catch (JSONException e)
            {
                e.PrintStackTrace();
            }
        }

        private void spinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;
            searchButton.Visibility = ViewStates.Visible;
        }

        public void hideSoftKeyboard()
        {
            var currentFocus = this.CurrentFocus;
            if (currentFocus != null)
            {
                InputMethodManager inputMethodManager = (InputMethodManager)this.GetSystemService(Context.InputMethodService);
                inputMethodManager.HideSoftInputFromWindow(currentFocus.WindowToken, 0);
            }
        }

        private string parseQuery()
        {
            string parsedQuery="";
            string[] queryArray = goodEditText.Query.Split(' ');
            parsedQuery = "WHERE NAIM LIKE '%25" + queryArray[0] + "%'";
            for (int i = 1; i < queryArray.Length; i++)
            {
                parsedQuery += " and NAIM LIKE '%25" + queryArray[i] + "%'";
            }
            return parsedQuery;
        }

        private void Btn_Click(object sender, EventArgs args)
        {
            if (!flag)
            {
                flag = true;
                hideSoftKeyboard();

                if (goodEditText.Query != null && !goodEditText.Query.Equals(""))
                {
                    if ((obl.Checked && spinner.SelectedItem.ToString().Equals("Вся Беларусь")) || (zavod.Checked && spinner.SelectedItem.ToString().Equals("Все производители")))
                    {
                        Intent intent = new Intent(this, typeof(ExpandableActivity));
                        intent.PutExtra("query", parseQuery());
                        intent.PutExtra("searchFormat", "obl");
                        intent.PutExtra("credentials", spinner.SelectedItem.ToString());
                        StartActivity(intent);
                    }
                    else
                    {
                        Intent intent = new Intent(this, typeof(ResultActivity));
                        intent.PutExtra("query", parseQuery());
                        if (obl.Checked)
                        {
                            intent.PutExtra("searchFormat", "obl");
                        }
                        else
                        {
                            foreach (Manufacture m in manufactures)
                            {
                                if (m.Norg == spinner.SelectedItem.ToString())
                                {
                                    intent.PutExtra("searchFormat", m.Korg.ToString());
                                }
                            }
                        }
                        intent.PutExtra("credentials", spinner.SelectedItem.ToString());
                        StartActivity(intent);
                    }
                }
                else
                {
                    Toast.MakeText(this, "Введите название искомого товара", ToastLength.Long).Show();
                    flag = false;
                }
            }            
        }
    }
}