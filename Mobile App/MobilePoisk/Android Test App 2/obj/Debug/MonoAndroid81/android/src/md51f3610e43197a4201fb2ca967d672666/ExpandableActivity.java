package md51f3610e43197a4201fb2ca967d672666;


public class ExpandableActivity
	extends android.app.Activity
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("Android_Test_App_2.ExpandableActivity, Android Test App 2", ExpandableActivity.class, __md_methods);
	}


	public ExpandableActivity ()
	{
		super ();
		if (getClass () == ExpandableActivity.class)
			mono.android.TypeManager.Activate ("Android_Test_App_2.ExpandableActivity, Android Test App 2", "", this, new java.lang.Object[] {  });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
