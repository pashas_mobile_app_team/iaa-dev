﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using System;
using Android.Widget;
using Android.Content;
using Org.Json;
using Android.Content.PM;
using System.Threading.Tasks;

namespace Android_Test_App_2
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true, Icon = "@mipmap/ivc_logo_png", ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : AppCompatActivity
    {
        private JSONParser jParser = new JSONParser();
        private String urlCountViaAllAreas = "http://ips.mshp.gov.by/andr_mob/countViaAllAreas.php";
        private String TAG_PRODUCTS = "products";
        private String TAG_CNT = "CNT";
        private TextView goodText;
        private ImageButton searchIcon;
        private ImageButton statButton;
        private static bool flagSearch = false;
        private static bool flagStat = false;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.startPage);
            searchIcon = FindViewById<ImageButton>(Resource.Id.searchIcon);
            statButton = FindViewById<ImageButton>(Resource.Id.stat);
            goodText = FindViewById<TextView>(Resource.Id.statcount);
            getCount();
            statButton.Click += StatButton_Click;
            searchIcon.Click += SearchIcon_Click;

            flagSearch = false;
            flagStat = false;
        }

        public override void OnBackPressed()
        {
            Intent startMain = new Intent(Intent.ActionMain);
            startMain.AddCategory(Intent.CategoryHome);
            startMain.SetFlags(ActivityFlags.NewTask);
            
            RunOnUiThread(
                async () =>
                {
                    var isCloseApp = await AlertAsync(this, "ИПС \"Техсервис\"", "Вы действительно хотите выйти?", "Да", "Нет");

                    if (isCloseApp)
                    {
                        StartActivity(startMain);
                    }
                });
        }

        public Task<bool> AlertAsync(Context context, string title, string message, string positiveButton, string negativeButton)
        {
            var tcs = new TaskCompletionSource<bool>();

            using (var db = new Android.App.AlertDialog.Builder(context))
            {
                db.SetTitle(title);
                db.SetMessage(message);
                db.SetPositiveButton(positiveButton, (sender, args) => { tcs.TrySetResult(true); });
                db.SetNegativeButton(negativeButton, (sender, args) => { tcs.TrySetResult(false); });
                db.Show();
            }

            return tcs.Task;
        }

        protected override void OnResume()
        {
            base.OnResume();
            flagSearch = false;
            flagStat = false;
        }

        protected override void OnRestart()
        {
            base.OnRestart();
            flagSearch = false;
            flagStat = false;
        }

        private void SearchIcon_Click(object sender, EventArgs e)
        {
            if (!flagSearch)
            {
                flagSearch = true;
                Intent intent = new Intent(this, typeof(SearchActivity));
                StartActivity(intent);
            }
        }

        private void StatButton_Click(object sender, EventArgs e)
        {
            if (!flagStat)
            {
                flagStat = true;
                Intent intent = new Intent(this, typeof(StatisticsActivity));
                StartActivity(intent);
            }
        }

        private void getCount()
        {
            JSONObject jobj = null;
            jobj = jParser.makeHttpRequest(urlCountViaAllAreas);
            if (jobj == null)
            {
                return;
            }
            jobj=jobj.GetJSONArray(TAG_PRODUCTS).GetJSONObject(0);
            goodText.Text = "Всего позиций: " + jobj.GetInt(TAG_CNT).ToString();
        }
    }
}

