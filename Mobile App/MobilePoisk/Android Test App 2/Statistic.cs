﻿namespace Android_Test_App_2
{
    class Statistic
    {
        private string name;
        private string date;
        private string area;
        private int count;

        public Statistic(string name, string date, string area, int count)
        {
            this.name = name;
            this.date = date;
            this.area = area;
            this.count = count;
        }

        public string Name { get => name; set => name = value; }
        public string Date { get => date; set => date = value; }
        public string Area { get => area; set => area = value; }
        public int Count { get => count; set => count = value; }
    }
}