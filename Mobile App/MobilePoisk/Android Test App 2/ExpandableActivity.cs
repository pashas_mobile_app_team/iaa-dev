﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Widget;
using Org.Json;

namespace Android_Test_App_2
{
    [Activity(Label = "ExpandableActivity", ScreenOrientation = ScreenOrientation.Portrait)]
    public class ExpandableActivity : Activity
    {
        public ProgressDialog progress;
        private ExpandableListView view;
        private String urlGetGoodsViaAllAreas = "http://ips.mshp.gov.by/andr_mob/getGoodsViaAllAreas.php";
        private String urlCountViaAllAreas = "http://ips.mshp.gov.by/andr_mob/countViaAllAreas.php";
        private String urlGetGoodsViaAllfactory = "http://ips.mshp.gov.by/andr_mob/getGoodsViaAllFactory.php";
        private String urlCountViaAllFactory = "http://ips.mshp.gov.by/andr_mob/countViaAllFactory.php";
        private String urlSearchForWarehouses = "http://ips.mshp.gov.by/andr_mob/searchForWarehouses.php";
        public String query = "";
        private string credentials;
        private int countL = 0;
        // JSON Node names
        private String TAG_PRODUCTS = "products";
        private String TAG_KOL = "KOL";
        private String TAG_NAME = "NAIM";
        private String TAG_CENA = "CENA";
        private String TAG_WAREHOUSE = "WAREHOUSE";
        private String TAG_PROVINCE = "PROVINCE";
        private String TAG_NORG = "NORG";
        private String TAG_TEL = "TEL";
        private String TAG_ADRESS = "ADRESS";
        private String TAG_MAILADR = "MAILADR";
        private String TAG_UNN = "UNN";
        private String TAG_RESPONS = "RESPONS";
        private String TAG_ADS = "ADS";
        private String TAG_NAMEW = "NAMEW";
        private String TAG_ADDRESSW = "ADDRESSW";
        private String TAG_CNT = "CNT";
        private JSONParser jParser = new JSONParser();
        JSONArray products = null;
        Dictionary<int, Product> parameters = new Dictionary<int, Product>();

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.expand);
            try
            {
                query = Intent.GetStringExtra("query");
                credentials = Intent.GetStringExtra("credentials");
                view = FindViewById<ExpandableListView>(Resource.Id.expresults);
                createProgressBar();
                getCount();
                view.ChildClick += View_ChildClick;
                await doInBackgroundAsync();
                view.SetAdapter(new ExpandableProductAdapter(GetProducts(), credentials));
            }
            catch (Exception e)
            {
                Toast.MakeText(this, "ExpandableActivity", ToastLength.Long).Show();
            }
        }

        private void createProgressBar()
        {
            progress = new ProgressDialog(this);
            progress.Indeterminate = true;
            progress.SetProgressStyle(ProgressDialogStyle.Spinner);
            progress.SetMessage("Пожалуйста, подождите. Идет загрузка...");
            progress.SetCancelable(false);
        }

        private async Task doInBackgroundAsync()
        {
            progress.Show();
            Task task = new Task(doInBackground);
            task.Start();
            await task;
            progress.Hide();
        }

        private void View_ChildClick(object sender, ExpandableListView.ChildClickEventArgs e)
        {
            Product product = null;
            Dictionary<string, List<Product>> pairs;
            pairs = ExpandableProductAdapter._sections;
            string key = "";
            int section = 0;

            foreach (KeyValuePair<string, List<Product>> keyValue in pairs)
            {
                if (product != null)
                {
                    break;
                }
                for (int i = 0; i < keyValue.Value.Count; i++)
                {
                    if (product != null)
                    {
                        break;
                    }
                    if (i == e.ChildPosition && section == e.GroupPosition)
                    {
                        key = keyValue.Key;

                        product = keyValue.Value[i]; break;
                    }
                }
                section++;
            }
            if (product != null && !product.Name.Equals("Нет указанных товаров"))
            {
                Intent intent = new Intent(this, typeof(AboutProductActivity));
                intent.PutExtra("product", e.ChildPosition + 1);
                intent.PutExtra("credentials", credentials);
                intent.PutExtra("section", key);
                StartActivity(intent);
            }
        }

        //[Android.Runtime.Register("onStart", "()V", "GetOnStartHandler")]
        //protected override void OnStart()
        //{
        //    base.OnStart();
        //    try
        //    {
        //        doInBackground();
        //        view.SetAdapter(new ExpandableProductAdapter(GetProducts()));
        //    }
        //    catch (Exception e)
        //    {
        //        Toast.MakeText(this, "При попытке получения товаров что-то пошло не так", ToastLength.Long).Show();
        //    }
        //}

        private void getCount()
        {
            JSONObject jobj = null;
            if (credentials.Equals("Вся Беларусь"))
            {
                jobj = jParser.makeHttpRequest(urlCountViaAllAreas + "/?query=" + query).GetJSONArray(TAG_PRODUCTS).GetJSONObject(0);
            }
            else
            {
                jobj = jParser.makeHttpRequest(urlCountViaAllFactory + "/?query=" + query).GetJSONArray(TAG_PRODUCTS).GetJSONObject(0);
            }
            if (jobj == null)
            {
                countL = 0;
                return;
            }
            countL = jobj.GetInt(TAG_CNT);
        }

        IList<Product> GetProducts()
        {
            var list = new List<Product>();
            foreach (KeyValuePair<int, Product> keyValue in parameters)
            {
                list.Add(keyValue.Value);
            }
            if (list.Count == 0)
            {
                list.Add(new Product("Нет указанных товаров", "Ошибка", "Ошибка", 0, "", "", 0, "", "", "", "", "", 0));
            }
            return list;
        }

        public void doInBackground()
        {
            JSONObject json = null;
            if (credentials.Equals("Вся Беларусь"))
            {
                json = jParser.makeHttpRequest(urlGetGoodsViaAllAreas + "/?query=" + query);
            }
            else
            {
                json = jParser.makeHttpRequest(urlGetGoodsViaAllfactory + "/?query=" + query);
            }

            if (json == null)
            {
                return;
            }
            try
            {
                products = json.GetJSONArray(TAG_PRODUCTS);
                for (int i = 0; i < products.Length(); i++)
                {
                    JSONObject c = products.GetJSONObject(i);

                    String name = c.GetString(TAG_NAME);
                    String cena = c.GetString(TAG_CENA);
                    int kol = c.GetInt(TAG_KOL);
                    String warehouseID = c.GetString(TAG_WAREHOUSE);
                    String norg = c.GetString(TAG_NORG);
                    String tel = c.GetString(TAG_TEL);
                    String adress = c.GetString(TAG_ADRESS);
                    String mailadr = c.GetString(TAG_MAILADR);
                    int unn = c.GetInt(TAG_UNN);
                    String respons = c.GetString(TAG_RESPONS);
                    String ads = c.GetString(TAG_ADS);
                    int provinceID = c.GetInt(TAG_PROVINCE);
                    String namew = "", addressw = "";
                    int ware = 0;
                    try
                    {
                        if (warehouseID != "null")
                        {
                            ware = int.Parse(warehouseID);
                            JSONObject json2 = jParser.makeHttpRequest(urlSearchForWarehouses + "/?query=" + warehouseID);
                            JSONArray array = json2.GetJSONArray("warehouses");

                            namew = array.GetJSONObject(0).GetString(TAG_NAMEW);
                            if (array.GetJSONObject(0).GetString(TAG_ADDRESSW) != "0")
                            {
                                addressw = array.GetJSONObject(0).GetString(TAG_ADDRESSW);
                            }
                            else
                            {
                                addressw = adress;
                            }
                        }
                        else
                        {
                            addressw = adress;
                        }
                    }
                    catch (Exception)
                    {
                        addressw = adress;
                    }
                        string province = "";
                        switch (provinceID)
                        {
                            case 1: province = "Брестская область"; break;
                            case 2: province = "Витебская область"; break;
                            case 3: province = "Гомельская область"; break;
                            case 4: province = "Гродненская область"; break;
                            case 5: province = "Минская область"; break;
                            case 6: province = "Могилевская область"; break;
                            case 7: province = "Город Минск"; break;
                        }
                        parameters.Add(i + 1, new Product(name, norg, province, kol, cena, namew, unn, tel, addressw, mailadr, respons, ads, ware));
                   
                }
            }
            catch (JSONException e)
            {
                e.PrintStackTrace();
            }
            catch (Exception e)
            {
                //Toast.MakeText(this, "doinbackgr", ToastLength.Long).Show();
            }
        }
    }
}