﻿using System.Collections.Generic;
using System.Linq;
using Android.Views;
using Android.Widget;

namespace Android_Test_App_2
{
    class ExpandableProductAdapter : BaseExpandableListAdapter
    {
        protected IList<Product> _products;
        public static Dictionary<string, List<Product>> _sections;
        protected string cred;

        public ExpandableProductAdapter(IList<Product> products,string cred)
        {
            _products = products;
            this.cred = cred;
            _sections = new Dictionary<string, List<Product>>();
            BuildSections();
        }

        public override int GroupCount
        {
            get
            {
                return _sections.Keys.Count;
            }
        }

        public override bool HasStableIds
        {
            get
            {
                return false;
            }
        }

        public override Java.Lang.Object GetChild(int groupPosition, int childPosition)
        {
            return null;
        }

        public override long GetChildId(int groupPosition, int childPosition)
        {
            return 0;
        }

        public override int GetChildrenCount(int groupPosition)
        {
            var sect = _sections.Keys.ToList()[groupPosition];
            return _sections[sect].Count;
        }

        public override View GetGroupView(int position, bool isExpandable, View convertView, ViewGroup parent)
        {
            GroupViewHolder holder = null;
            var view = convertView;
            if (view != null) holder = view.Tag as GroupViewHolder;
            if (holder == null)
            {
                view = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.row_expandable_header, null);
                holder = new GroupViewHolder();
                holder.Text = view as CheckedTextView;
            }
            string name="";
            var sect = _sections.Keys.ToList()[position];
            if (cred.Equals("Все производители")){
                name = _sections[sect].First().Factory;
            }
            else
            {
                name = _sections[sect].First().Area;
            }
            holder.Text.Text = name;
            holder.Text.Checked = isExpandable;
            return view;
        }

        public override View GetChildView(int groupPosition, int childPosition, bool isLastChild, View convertView, ViewGroup parent)
        {
            var view = convertView;
            var sect = _sections.Keys.ToList()[groupPosition];
            var item = _sections[sect][childPosition];
            if (view == null)
            {
                view = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.productLayout, parent, false);
                var viewHolder = new ChildViewHolder();
                viewHolder.Text = view.FindViewById<TextView>(Resource.Id.row_custom_name);
                view.Tag = viewHolder;
            }
            var holder = (ChildViewHolder)view.Tag;
            holder.Text.Text = item.Name;
            return view;
        }

        public override Java.Lang.Object GetGroup(int groupPosition)
        {
            return null;
        }

        public override long GetGroupId(int groupPosition)
        {
            return groupPosition;
        }

        public override bool IsChildSelectable(int groupPosition, int childPosition)
        {
            return true;
        }

        void BuildSections()
        {
            _sections.Clear();
            if(cred.Equals("Все производители"))
            {
                foreach (var s in _products.GroupBy(item => item.Factory).OrderBy(gr => gr.Key))
                {
                    _sections.Add(s.Key, s.ToList());
                }
            }
            else
            {
foreach (var s in _products.GroupBy(item => item.Area).OrderBy(gr => gr.Key))
            {
                _sections.Add(s.Key, s.ToList());
            }
            }
        }

        class GroupViewHolder : Java.Lang.Object
        {
            public CheckedTextView Text { get; set; }
        }

        class ChildViewHolder : Java.Lang.Object
        {
            public TextView Text { get; set; }
            public TextView Weight { get; set; }
            public ImageView Icon { get; set; }
        }
    }
}