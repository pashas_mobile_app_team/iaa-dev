﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Android.Accounts;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Widget;

namespace Android_Test_App_2
{
    [Activity(Label = "AboutProductActivity", ScreenOrientation = ScreenOrientation.Portrait, WindowSoftInputMode = Android.Views.SoftInput.AdjustPan)]
    public class AboutProductActivity : Activity
    {
        private static bool flagOrder = false;
        private static bool flagOrderImage = false;
        private static bool flagHome = false;
        private ListView view;
        private ImageButton order;
        private Button orderButton;
        private Button home;
        private EditText fio;
        private EditText emailContact;
        private EditText orderCount;
        private Dictionary<string, List<Product>> pairs;
        private static String urlInsert = "http://ips.mshp.gov.by/andr_mob/insertOrder.php";

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.aboutProduct);
            view = FindViewById<ListView>(Resource.Id.aboutProduct);
            string keySection = Intent.GetStringExtra("section");
            int productID = Intent.GetIntExtra("product", 0);
            string cred = Intent.GetStringExtra("credentials");
            Product product = null;
            int temp = 1;
            flagOrder = false;
            flagOrderImage = false;
            flagHome = false;

            if (cred == "Вся Беларусь")
            {
                pairs = ExpandableProductAdapter._sections;
                foreach (KeyValuePair<string, List<Product>> keyValue in pairs)
                {
                    if (product != null)
                    {
                        break;
                    }
                    for (int i = 0; i < keyValue.Value.Count; i++)
                    {
                        if (product != null)
                        {
                            break;
                        }
                        if (i == productID - 1 && keySection == keyValue.Key)
                        {
                            product = keyValue.Value[i]; break;
                        }
                    }
                }
            }
            else
            {
                pairs = SectionFactoryAdapter._sections;
                foreach (KeyValuePair<string, List<Product>> keyValue in pairs)
                {
                    if (product != null)
                    {
                        break;
                    }
                    for (int i = 0; i < keyValue.Value.Count; i++)
                    {
                        if (product != null)
                        {
                            break;
                        }
                        if (temp == productID)
                        {
                            product = keyValue.Value[i]; break;
                        }
                        temp++;
                    }
                    temp++;
                }
            }
            order = FindViewById<ImageButton>(Resource.Id.order);
            if (product.Email.Equals("一")) order.Visibility = Android.Views.ViewStates.Gone;
            if (product != null)
            {
                List<string> list = new List<string>();
                list.AddRange(new string[]
                {
                "Товар: " + product.Name,
                "Поставщик: " + product.Factory,
                "Склад: " + product.Warehouse,
                "УНН: " + product.Ynn,
                "Телефон: " + product.Phone,
                "Адрес: " + product.Address,
                "Контактные лица: " + product.Contacts,
                "В наличии: " + product.Count,
                "Цена: " + product.Price
                });
                var adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, list);
                view.Adapter = adapter;
                order.Click += delegate
                {
                    try
                    {
                        if (!flagOrderImage)
                        {
                            flagOrderImage = true;
                            SetContentView(Resource.Layout.orderLayout);
                            fio = FindViewById<EditText>(Resource.Id.fioEdit);
                            emailContact = FindViewById<EditText>(Resource.Id.emailEdit);
                            orderCount = FindViewById<EditText>(Resource.Id.countEdit);
                            orderButton = FindViewById<Button>(Resource.Id.orderButton);
                            getAccounts();
                            int count;
                            orderButton.Click += delegate
                            {
                                if (!flagOrder)
                                {
                                    flagOrder = true;
                                    if (!fio.Text.Equals("") && !emailContact.Text.Equals("") && Regex.IsMatch(emailContact.Text, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"))
                                    {
                                        if (int.TryParse(orderCount.Text, out count) && (count > 0 && count <= product.Count))
                                        {
                                            var email = new Intent(Intent.ActionSend);
                                            email.PutExtra(Intent.ExtraEmail, new string[] { product.Email });
                                            email.PutExtra(Intent.ExtraSubject, "Заказ");
                                            email.PutExtra(Intent.ExtraText, "Здраствуйте. Мы бы хотели заказать у вас " + product.Name + "в количестве " + orderCount.Text + "шт");
                                            email.SetType("message/rfc822");
                                            StartActivity(email);
                                            new JSONParser().makeHttpRequestWithoutResponse(urlInsert + "/?date='" + DateTime.Now.ToString("u").Remove(DateTime.Now.ToString("u").Length - 1) + "'&name='" + product.Name + "'&count=" + product.Count + "&price=" + product.Price + "&cname='" + fio.Text + "'&ccontact='" + emailContact.Text + "'&warehouse=" + product.WarehouseID);
                                        }
                                        else
                                        {
                                            Toast.MakeText(this, "Выберите нормальное число товаров", ToastLength.Long).Show();
                                            flagOrder = false;
                                        }
                                    }
                                    else
                                    {
                                        Toast.MakeText(this, "Введите контактные данные", ToastLength.Long).Show();
                                        flagOrder = false;
                                    }
                                }

                            };
                            home = FindViewById<Button>(Resource.Id.home);
                            home.Click += delegate
                            {
                                if (!flagHome)
                                {
                                    flagHome = true;
                                    Intent intent = new Intent(this, typeof(MainActivity));
                                    StartActivity(intent);
                                }
                            };
                        }
                    }
                    catch (Exception e)
                    {
                        Toast.MakeText(this, "При формировании заказа произошла ошибка", ToastLength.Long).Show();
                    }
                };
            }
        }

        protected override void OnResume()
        {
            base.OnResume();
            flagOrder = false;
            flagOrderImage = false;
            flagHome = false;
        }

        protected override void OnRestart()
        {
            base.OnRestart();
            flagOrder = false;
            flagOrderImage = false;
            flagHome = false;
        }

        private void getAccounts()
        {
            Account[] accounts;
                accounts = AccountManager.Get(this).GetAccounts();
            List<string> emails = new List<string>();
            try
            {
                foreach (Account account in accounts)
                {
                    if(Regex.IsMatch(account.Name, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"))
                    {
                        emailContact.Text = account.Name;
                        break;
                    }

                }
            }
            catch (Exception e)
            {
                //Log.i("Exception", "Exception:" + e);
            }
        }
    }
}