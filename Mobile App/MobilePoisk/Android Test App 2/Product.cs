﻿using System;

namespace Android_Test_App_2
{
    class Product
    {
        private string name;
        private string factory;
        private string area;
        private int count;
        private string price;
        private string warehouse;
        private int ynn;
        private string phone;
        private string address;
        private string email;
        private string contacts;
        private string warning;
        private int warehouseID;

        public Product(string name, string factory, string area, int count, string price, string warehouse, int ynn, string phone, string address, string email, string contacts, string warning, int warehouseID)
        {
            this.name = (name == "null") ? "一" : name;
            this.factory = (factory == "null") ? "一" : factory;
            this.area = (area == "null") ? "一" : area;
            this.count = (count == 0) ? 0 : count;
            this.price = (price == "null") ? "一" : price;
            this.warehouse = (warehouse == "null" || warehouse == null || warehouse == "") ? "一" : warehouse;
            this.ynn = (ynn == 0) ? 0 : ynn;
            this.phone = (phone == "null") ? "一" : phone;
            this.address = (address == "null") ? "一" : address; ;
            this.email = (email == "null") ? "一" : email;
            this.contacts = (contacts == "null") ? "一" : contacts;
            this.warning = (warning == "null") ? "一" : warning;
            this.warehouseID = (warehouseID == 0) ? 0 : warehouseID;
        }

        public string Name { get => name; set => name = value; }
        public string Factory { get => factory; set => factory = value; }
        public string Area { get => area; set => area = value; }
        public int Count { get => count; set => count = value; }
        public string Price { get => price; set => price = value; }
        public string Warehouse { get => warehouse; set => warehouse = value; }
        public int Ynn { get => ynn; set => ynn = value; }
        public string Phone { get => phone; set => phone = value; }
        public string Address { get => address; set => address = value; }
        public string Email { get => email; set => email = value; }
        public string Contacts { get => contacts; set => contacts = value; }
        public string Warning { get => warning; set => warning = value; }
        public int WarehouseID { get => warehouseID; set => warehouseID = value; }

        public override string ToString()
        {
            return String.Format("{0}", name);
        }

        public string[] productInfo()
        {
            string[] vs = new string[] {
            name,
            factory,
            area,
            count.ToString(),
            price,
            warehouse,
            ynn.ToString(),
            phone,
            address,
            email,
            contacts,
            warning };
            return vs;
        }
    }
}
