<?php
$response = array();

require 'db_connect.php';
$db = new DB_CONNECT();
mysql_query("SET NAMES utf8");
$query=$_GET["query"];

$result = mysql_query("SELECT sporg.NORG, sporg.update_date, COUNT(sporg.`KORG`) AS items_count,sporg.`province` 
FROM sporg LEFT JOIN ostat 
ON sporg.`KORG` = ostat.`RAPS` 
GROUP BY sporg.`KORG` 
ORDER BY sporg.`province`, items_count DESC") or die(mysql_error());

if (mysql_num_rows($result) > 0) {
    $response["statistics"] = array();

    while ($row = mysql_fetch_array($result)) {
        $statistic = array();
        $statistic["NORG"] = $row["NORG"];
        $statistic["DATE"] = $row["update_date"];
        $statistic["COUNT"] = $row["items_count"];
        $statistic["PROVINCE"] = $row["province"];

        array_push($response["statistics"], $statistic);
    }
    $response["success"] = 1;

    echo json_encode($response);
} else {
    $response["success"] = 0;
    $response["message"] = "No statistics found";

echo json_encode($response);
}
?>