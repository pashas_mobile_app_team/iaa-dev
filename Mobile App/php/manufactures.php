<?php
$response = array();

require 'db_connect.php';
$db = new DB_CONNECT();

$result = mysql_query("SELECT sporg.NORG,sporg.KORG
FROM sporg
WHERE sporg.is_manufacturer=1") or die(mysql_error());

// mysql_query("SET NAMES 'UTF8'; ");

if (mysql_num_rows($result) > 0) {
    $response["manufactures"] = array();

    while ($row = mysql_fetch_array($result)) {
        $manufactire = array();
        $manufactire["NORG"] = $row["NORG"];
        $manufactire["KORG"] = $row["KORG"];

        array_push($response["manufactures"], $manufactire);
    }
    $response["success"] = 1;

    echo json_encode($response);
} else {
    $response["success"] = 0;
    $response["message"] = "No manufactures found";

  echo json_encode($response);
}
?>